<?php
//Liina Abner - DK14
//keerab stringi tagurpidi
?>
<html>
<head>
<meta charset="UTF-8" />
<title>
Ülesanne 7 - php
</title>
</head>
<body>
<form name="getform" action="reverse.php" method="get">
<input type="text" name="reverse" placeholder="kirjuta midagi">
<input type="submit" value="Saada">
</form>
<?php
if(isset($_GET["reverse"]) && !empty($_GET["reverse"])){	//kas reverse parameeter on olemas ja pole tühi
	$in = strtolower($_GET["reverse"]);	//paneme kõik väikesteks tähtedeks
	$counter = -1;	//alustame viimasest tähest
	$out = "";		//teeme tühja stringi, et oleks millele otsa lisada
	while ($counter > -1-strlen($in)) {	//kuniks pole esimese täheni jõutud
		$out .= substr($in,$counter,1);	//lisame sisendstringi tähe väljundstringile
		$counter--;	//nihutame counterit edasi, st. tagasi
	}
	echo("Sisestasid: ".$in."<br>Tagurpidi: ".$out);
}
?>
</body>
</html>
