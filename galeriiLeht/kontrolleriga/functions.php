<?php
function get_pilt() {
	global $pildid;
	global $pilt;
	global $error;
	if (!empty($_POST['pilt'])) {
		if ($_POST['pilt'] >= 1 && $_POST['pilt'] <= count($pildid)) {
			$pilt = $_POST['pilt'];
		} else {
			$error = "Sellist pilti pole.";
		}
	} else {
		$error = "Pole midagi valitud.";
	}
}
function kontrolli_haaletamist() {
	global $error;
	global $pilt;
	global $info;
	if (isset($_SESSION['haaletas'])) {
		$error = "Sa juba hääletasid.";
	} elseif (isset($pilt)) {
		$_SESSION['haaletas'] = $pilt;
	}
}
?>