<!DOCTYPE html>
<!--Liina Abner DK14-->
<html>
<head>
	<meta charset="utf-8" />
    <style>
		.raamat {
			border:solid #c0c0c0 1px;
			padding: 10px;
			margin: 5px;
			width: 300px;
			height: 130px;
			display:inline-block;
		}
		img {
			width:100px;
			height:130px;
			padding-left: 10px;
			float:right;
		}
		
		.autor {
			font-style:oblique;
		}
		.pealkiri {
			font-size:x-large;
			color: darkred;
		}
		.sisu {
			font-family:sans-serif;
			margin-top:20px;
			font-size:smaller;
		}
    </style>
</head>
<body>
<?php
$riiul = array( 
			array(
				'pealkiri'=>'Bullerby lapsed',
				'autor'=>'Astrid Lindgren',
				'sisu'=>'Toredad lood lapsepõlvest Rootsis',
				'lk'=>256,
				'pilt'=>'bullerby.jpg'
			),
			array(
				'pealkiri'=>'Kirjud pildid',
				'autor'=>'Tundmatu autor',
				'sisu'=>'Vanaaegsed kirjud pildid häädele lastele',
				'lk'=>25,
				'pilt'=>'kirjud.jpg'
			),
			array(
				'pealkiri'=>'Otje',
				'autor'=>'Annie M.G. Schmidt',
				'sisu'=>'Lugu ühest tüdrukust, kes oma kokast isaga ringi seikles',
				'lk'=>200,
				'pilt'=>'otje.jpg'
			),
			array(
				'pealkiri'=>'Popo ja Fifina',
				'autor'=>'Bonteps, Arna',
				'sisu'=>'Paljasjalgsete Haiiti laste argipäev',
				'lk'=>34,
				'pilt'=>'popo.jpg'
			)
		);
foreach ($riiul as $raamat) {
	include('raamat.html');
}
?>
</body>
</html>