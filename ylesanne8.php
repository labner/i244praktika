<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>
8 ülesanne
</title>
<?php
//tekitame massiivi, kus on vaikimis väärtused
$params = array(
	'bgcolor'=>"#ff8000",
	'color'=>"#808000",
	'piir'=>"1",
	'text'=>"Sisesta oma näidistekst",
	'raadius'=>"3",
	'font-family'=>"arial"
);
 
/*
kui vorm on saadetud, siis vaatab, millised parameetrid tulid POST meetodiga
ja kirjutab vaikimisi parameetrite massiivis väärtused üle
*/
if(isset($_GET['vorm'])) {
	foreach ($_POST as $key=>$value){
		$params[$key] = htmlspecialchars($value);
	}
	//print_r($params);
}
?>
<style>
body {
	position:relative;
}
div {
	margin:100px;
}
#out {
	background:<?php echo($params['bgcolor']); ?>;
	color:<?php echo($params['color']); ?>;
	border: solid <?php echo($params['piir']); ?>px;
	border-radius: <?php echo($params['raadius']); ?>px;
	position:absolute;
	padding:10px;
	font-family:<?php echo($params['font-family']); ?>;
	font-size:larger;
	display:inline-block;
	position:relative;
	left:100px;
	bottom:100px
}
form {
	border: solid 1px #c0c0c0;
	display:inline-block;
	font-family:arial;
	font-size:smaller;
	padding:3px;
	text-align:right;
}
input,select {
	width:5em;
	margin-left:15px
}
input[type=number] {
	background:pink;
	width:4.5em
}
</style>
</head>
<body>
<div>
	<form action="ylesanne8.php?vorm" method="post">
	<textarea name="text"><?php echo $params['text']; ?></textarea><br>
	Font:
	 <select class="input" name="font-family">
		<option value="arial" <?php if ($params['font-family'] == 'arial') { echo " selected";} ?>>Arial</option>
		<option value="times" <?php if ($params['font-family'] == 'times') { echo " selected";} ?>>Times</option>
	</select><br>
	Teksti värv:
	<input class="input" type="color" name="color" value="<?php echo $params['color']; ?>"/><br>
	Tausta värv:
	<input class="input" type="color" name="bgcolor" value="<?php echo $params['bgcolor']; ?>"/><br>
	Piiri paksus:
	<input class="input" type="number" name="piir" value="<?php echo $params['piir']; ?>" min="1" max="5" size="2" /><br>
	Ümarus:
	<input class="input" type="number" name="raadius" size="2" value="<?php echo $params['raadius']; ?>" min="0" max="25" /><br>
	<input class="input" type="submit" value="Vaata" />
	</form>
	<div id="out">
		<?php echo($params['text']); ?>
	</div>
</div>
</body>
</html>